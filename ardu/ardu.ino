#include <adns50x0.h>
#include <Adns5050.h>
#include <Mouse.h>

const int upButton = A3;
const int downButton = A5;
const int lftClick = A1;
const int rgtClick = 9;
const int ncs = 2;
const int nreset = 3;

int upB, downB, rgtC, lftC;

int lupBState = LOW;
int ldownBState = LOW;

unsigned long lastUpDebounceTime = 0;
unsigned long lastDownDebounceTime = 0;
unsigned long debounceDelay = 50;


int increment_X;
int increment_Y;

Adns5050 adns(ncs, nreset);

void setup() {
  // put your setup code here, to run once:
  pinMode(upButton, INPUT);
  pinMode(downButton, INPUT);
  pinMode(lftClick, INPUT);
  pinMode(rgtClick, INPUT);
  Serial.begin(9600);
  Mouse.begin();
  adns.begin();
}

void loop() {
  // put your main code here, to run repeatedly:

  increment_X = -adns.read(Delta_X);
  increment_Y = adns.read(Delta_Y);
  Mouse.move(increment_X, increment_Y);
  int readingUp = digitalRead(upButton);
  int readingDown = digitalRead(downButton);
  rgtC = digitalRead(rgtClick);
  lftC = digitalRead(lftClick);
  if (lftC == HIGH and rgtC == HIGH) {
    Mouse.move(0,0,increment_X);
  } else if (lftC == HIGH) {
    Mouse.press(MOUSE_LEFT);
    while (lftC == HIGH);
    Mouse.release(MOUSE_LEFT);
  } else if (rgtC == HIGH) {
    Mouse.press(MOUSE_RIGHT);
    while (rgtC == HIGH);
    Mouse.release(MOUSE_RIGHT);
  }
  if (readingUp != lupBState) {
    lastUpDebounceTime = millis();
  }
  if (readingDown != ldownBState) {
    lastDownDebounceTime = millis();
  }
  if ((millis() - lastUpDebounceTime) > debounceDelay) {
    if (readingUp != upB) {
      upB = readingUp;
      if (upB == HIGH) {
        Serial.print("u");
      }
    }
  }
  lupBState = readingUp;

  if ((millis() - lastDownDebounceTime) > debounceDelay) {
    if (readingDown != downB) {
      downB = readingDown;
      if (downB == HIGH) {
        Serial.print("d");
      }
    }
  }
  ldownBState = readingDown;

  
}
