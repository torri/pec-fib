---
sidebar_position: 4
---

# Programar

Un altre cop, al nostre [repositori](https://gitlab.com/torri/pec-fib/) trobaràs el codi que hem fet servir per a fer funcionar el ratolí.

Consta de dues parts:

## Codi Arduino 

A la carpeta Ardu, hi és el codi per a programar la placa d'Arduino, des de l'Arduino IDE pots obrir el nostre codi i escriure'l a la micro, a més a més, el pots ampliar i modificar al teu gust!!!!!!!!!

Aquest llegeix el valor del sensor (increment en x i en y) i ho tradueix en moviment per a que traslladar el ratolí de la pantalla. 

## Driver

A la carpeta Driver, hi és un script per a configurar els botons de'n MIQUEL, ja que són configurables. Executant 

```bash 
./config_driver.sh 
```

Et demana introduïr els valors ordenadament. 

L'altre arxiu, el driver com a tal (`driver.py`) l'hauries de tenir de fons sempre que connectis el ratolí per a que aquest acabi de funcionar correctament. Pensa que és molt fàcil configurar per a que s'executi en segon pla cada cop que facis boot del sistema. 

