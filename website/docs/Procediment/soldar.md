---
sidebar_position: 2
---

# Placa impresa

:::caution 

Abans de res, recorda que si ets molt petit i/o no has soldat mai, sempre pots demanar ajuda a algú amb més experiència! 
:::

Per el tema de la placa impresa, existeixen vàries opcions. Nosaltres hem passat per a totes, desde prototip en placa de "topos", a una placa impresa de prova en coure, a una placa impresa final encarregada per [internet](https://pcbway.com). 

El que vulguis fer tu ja depèn del nivell de "professionalitat" a la que vulguis arribar, els diners que et vulguis gastar, els recursos que tinguis disponibles i les ganes que tinguis de fer-ho tot tu mateix. No shame en ninguna d'elles! 

Per fer qualsevol d'aquestes, necessitaràs tenir a mà l'esquema de connexions: 

![connect](/img/schematic.jpg)


## Placa de topos

Aquesta és la més senzilla i és perfecte per a practicar a soldar, tot i que no sol ser el producte final, sinó que es fa servir per a prototipar i trastejar amb components. 

Només necessites una placa de "topos", un soldador, estany per a soldar i els components necessaris. Un exemple de placa feta d'aquesta manera és: 

![Topitos](/img/topos.jpg)


## Placa de coure

Si tens una màquina que imprimeix sobre plaques de coure, pots aprofitar i imprimir-t'ho tu mateix! Tens [aquí](https://gitlab.com/torri/pec-fib) l'arxiu de KiCad, on trobaràs el nostre disseny passat i col·locat a una placa, on podràs extreure els gerbers i passar-los a la màquina. 

Aquesta tècnica també se sol fer a l'hora de prototipar, quan ja tens un disseny de circuit clar i vols provar com quedaria imprès. Tot i que no sigui pensat per a un producte final, té suficient cara i ulls com per a fer-lo servir així. 

Un exemple de placa feta d'aquesta manera és: 

![coure](/img/coure.jpg)


## Placa impresa

Nosaltres al final vam encarregar la placa impresa a través de [internet](https://pcbway.com). Només els hi has d'enviar els arxius gerbers i ells t'ho envien a casa, tot i que compte que amb l'empresa de paqueteria que trieu que nosaltres vam tenir problemes amb aduanes!!!! 

El resultat va ser: 

![impres](/img/impres.jpg)
