---
 sidebar_position: 1
---

# Quins passos has de seguir?  

Un cop tinguis tot el material necessari, hauràs de soldar-ho, programar-ho i imprimir-ho tu mateix. 

No tens per a què fer-ho en aquest ordre en particular, de fet, ho pots fer com tu vulguis a mesura de que tinguis els recursos necessaris. 

Si tens algun dubte sempre ens pots obrir una Issue al nostre [repositori](https://gitlab.com/torri/pec-fib/). 

Molta sort! 

![solete](/img/source.gif)


