---
sidebar_position: 3
---

# Impressió 3D

Aquesta part no té gaire misteri, la idea és proporcionar-te un "casing", que és la part exterior de'n MIQUEL. 

Tens el model pujat al [repositori](https://gitlab.com/torri/pec-fib/-/blob/main/mouse_with_arduino_with_holes.blend), així que el pots imprimir tu mateix o demanar que t'ho imprimeixin.

Una captura del model: 

![casing](/img/casing_full_de_pana.jpg)

Nosaltres vam haver de fer vàries proves amb materials. Amb PLA, tot i ser més tou, era molt més fàcil de clicar els botons i apenes vam haver de treballar el model un cop imprès. 

Una foto de com va quedar, encaixat amb la placa impresa: 

![quasi](/img/casi_final.jpg)

Quan vam demanar per a que ens ho imprimíssin més "professionalment", va quedar un model molt rígid i hi ha hagut molt treball posterior llimant la peça per a que els botons superiors siguin clickables, apart de separar en dos la peça lateral. Això ja va a gustos. 

Aquí una foto de la peça final: 

![fin](/img/final.jpg)
