---
 sidebar_position: 1
 title: Introducció
---

# MIQUEL
## Mouse Interactiu Quotidià Univeritari Educatiu Lliure

[![Arch](https://i.imgur.com/WHd7IYC.png)](https://archlinux.org/)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

En MIQUEL és un ratolí fet íntegrament amb eines de programari i maquinari lliure tals com: 

- [Blender](https://www.blender.org/) - Programari per modelar en 3D 
- [KiCad](https://www.kicad.org/) - Programari per a dissenyar circuits i plaques impreses
- [Arduino](https://www.arduino.cc/) - Microxip de maquinari lliure

## Autors 

-  Joel Miarons - estudiant de Enginyeria de Computadors 
-  Helena Vela - estudiant de Enginyeria de Computadors

