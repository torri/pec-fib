---
 sidebar_position: 2
 title: Materials
---

# Quin material necessitaràs? 

Si vols fer el teu propi MIQUEL a casa o a l'escola, hauràs de comprar-te el material que et diem en aquesta taula (també la pots trobar a la memòria del nostre  [repositori](https://gitlab.com/torri/pec-fib/-/tree/main/)). Pensa que l'Arduino no té per què ser "oficial" (en pots trobar imitacions bones i barates per internet!), així et sortirà molt més barat! 


| Component                       | Quantitat | Link de compra                                                                                                                                                                                                                                                                                         | Preu     |   |
|---------------------------------|-----------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|---|
| Condensador electrolitic 4.7 uF | 1         | [https://es.farnell.com/capacitor-4.7](https://es.farnell.com/multicomp/mcgpr50v475m5x11/conden-4-7-f-50v-20/dp/9451374?st=condensador\%20electrolitico\%204.7\%20uf)                                                                                                                          | 0,0934 € |   |
| Condensador electrolitic 3.3 uF | 1         | [https://es.farnell.com/capacitor-3.3](https://es.farnell.com/multicomp/mcnp100v335m6-3x11/conden-3-3-f-100v-20/dp/1236688)                                                                                                                                                                       | 0,0557 € |   |
| Condensadors smd 0.1uf          | 2         | [https://es.farnell.com/capacitor-0.1](https://es.farnell.com/vishay/293d104x9050a2te3/conden-0-1uf-50v-tant-case-a/dp/3366035)                                                                                                                          | 0,514 €  |   |
| Resistencia smd 500 ohms        | 1         | [https://www.mouser.es/resistor-500-smd](https://www.mouser.es/ProductDetail/Vishay-Thin-Film/FC0402E5000FTBST0?qs=sGAEpiMZZMvdGkrng054tw8SY4Y1EuWUiWLcd62lsbY\%3D)                                                                                                                               | 0,0394 € |   |
| Resistencia smd 4.7k ohms       | 4         | [https://www.mouser.es/resistor-4.7k-simd](https://www.mouser.es/ProductDetail/Vishay-Dale/CRCW01004K70FREL?qs=sGAEpiMZZMvdGkrng054t7z4BkURc4LzyCi3zayIZRUTlHNSIeODQg\%3D\%3D)                                                                                                                    | 0,166 €  |   |
| Switches B3F-3150               | 2         | [https://www.digikey.es/switches-main](https://www.digikey.es/products/es?keywords=B3F-3150)                                                                                                                                                                                                      | 0.44€    |   |
| Switches D2FS-F-N-T             | 2         | [https://www.mouser.es/lateralus](https://www.mouser.es/ProductDetail/Omron-Electronics/D2FS-F-N-T?qs=sEr1OoMjgSAE73IX06aSBA\%3D\%3D\&mgh=1\&vip=1\&gclid=Cj0KCQjw1ouKBhC5ARIsAHXNMI-G5Jd7gGbxkkbpS7d-3tMWMyCP2KoqH7b9W5U\_xOrPNgBHwosDsMIaAtDOEALw\_wcB)                                         | 0,639 €  |   |
| ADNS 5050                       | 1         | [https://es.aliexpress.com/ADNS5050](https://es.aliexpress.com/item/32814695132.html?spm=a2g0o.productlist.0.0.56842e230Y5Yjo\&algo\_pvid=4ffdc790-3ae8-4472-8adc-0f76b5573059\&algo\_exp\_id=4ffdc790-3ae8-4472-8adc-0f76b5573059-4\&pdp\_ext\_f=\%7B\%22sku\_id\%22\%3A\%2264607909349\%22\%7D) | 1,95 €   |   |
| Lent òptica                     | 1         | [https://es.aliexpress.com/optica](https://es.aliexpress.com/item/32811821343.html?spm=a2g0o.productlist.0.0.20ab69c9gOFn6e\&algo\_pvid=529a1213-59e1-4f8a-9cf8-463962751ada\&algo\_exp\_id=529a1213-59e1-4f8a-9cf8-463962751ada-0\&pdp\_ext\_f=\%7B\%22sku\_id\%22\%3A\%2264542963593\%22\%7D)   | 0,77€    |   |
| Arduino Micro                   | 1         | [https://store.arduino.cc/micro](https://store.arduino.cc/products/arduino-micro)                                                                                                                                                                                                                 | 18 €     |   |
| Cost total:                     |           |                                                                                                                                                                                                                                                                                                        | 25.45 €  |   |


Aquí no s'inclou la placa impresa (tot i que al report sí), hi ha vàries opcions, depenent de les ganes que tinguis de tenir-ho tot fet per tu al 100%. El procediment per a fer cadascuna te l'expliquem a [Placa](/docs/Procediment/soldar). Però si triéssis decidir que te la imprimíssin, suma uns 25€ extra aproximadament. 

