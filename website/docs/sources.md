---
sidebar_position: 5
---

# Fonts

* Icones - favpng.com i https://www.svgrepo.com/

* Fotografies i captures de pantalla - pròpies

* Docusaurus - https://docusaurus.io/

* Switch botons laterals - [link](https://app.ultralibrarian.com/details/26beb3d9-c144-11ea-b5d0-0aebb021a1ea/Omron/B3F-3150)

* Switch botons principals - [link](https://www.snapeda.com/parts/D2F-FL/Omron%20Electronics%20Inc-EMC%20Div/view-part/)
