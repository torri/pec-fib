import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {
    title: 'Codi Obert',
    Svg: require('../../static/img/os.svg').default,
    description: (
      <>
        Codi i arxius oberts per a que tothom s'ho pugui mirar, inspeccionar i modificar al seu gust.
      </>
    ),
  },
  {
    title: 'Fet per aprendre',
    Svg: require('../../static/img/cole.svg').default,
    description: (
      <>
        És un projecte fàcil per a que tothom, a qualsevol nivell, aprengui sobre maquinari lliure.
      </>
    ),
  },
  {
    title: 'Amb intenció de crèixer!',
    Svg: require('../../static/img/brot.svg').default,
    description: (
      <>
        Qualsevol idea i/o suggerència és benvinguda!
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
       <div className="container">
         <div className="row">
           {FeatureList.map((props, idx) => (
             <Feature key={idx} {...props} />
           ))}
         </div>
       </div>
     </section>
    
  );
}
