import pyautogui
import serial
import webbrowser

arduino = serial.Serial(port='/dev/ttyACM0', baudrate=9600)
while True:
    action = arduino.read(1).decode('ascii')
    if action == 'u':
        pyautogui.hotkey('altleft','tab')
    elif action == 'd':
        webbrowser.open("https://gitlab.com/torri/pec-fib", new=0)
