#!/bin/bash

custom_spaces='        '

show_shortcuts () {
    echo "Choose the key of your shortcut:"
    cat << "EOF"
1.  Escape       2. Tab          3. CapsLock     4. Left Shift
5.  Left Ctrl    6. Super        7. Left Alt     8. Right Alt
9.  Right Ctrl  10. Right Shift 11. Enter       12. Backspace
13. Left Arrow  14. Right Arrow 15. Up Arrow    16. Down Arrow
17. Delete      18. Insert      19. Character    0. End shortcut
EOF
}

process_key() {
    case $1 in
        0)
            end=1
            ;;
        1)
            result='esc'
            ;;
        2)
            result='tab'
            ;;
        3)
            result='capslock'
            ;;
        4)
            result='shiftleft'
            ;;
        5)
            result='ctrlleft'
            ;;
        6)
            result='win'
            ;;
        7)
            result='altleft'
            ;;
        8)
            result='altright'
            ;;
        9)
            result='ctrlright'
            ;;
        10)
            result='shiftright'
            ;;
        11)
            result='enter'
            ;;
        12)
            result='backspace'
            ;;
        13)
            result='left'
            ;;
        14)
            result='right'
            ;;
        15)
            result='up'
            ;;
        16)
            result='down'
            ;;
        17)
            result='delete'
            ;;
        18)
            result='insert'
            ;;
        19)
            echo "Enter the key:"
            read -n 1 key
            echo " "
            result="${key}"
            ;;
        *)
            result=''
    esac
}

# TODO acabar de passar a pyautogui

create_shortcut() {
    end=''
    show_shortcuts 
    read key
    process_key $key
    array_keys=()
    while [ -z ${end} ] ; do
        array_keys+=("$result")
        show_shortcuts
        read key
        process_key $key
    done
    hotkey=''
    for i in "${array_keys[@]}"; do
        hotkey="${hotkey},'${i}'"
    done

    hotkey="${hotkey:1}"

    echo -e "${custom_spaces}pyautogui.hotkey(${hotkey})" >> ${file}


}

select_opt() {
case $1 in
    1)
        echo "Url option selected"
        echo " "
        read -p "Enter your url: " url
        echo -e "${custom_spaces}webbrowser.open('${url}', new=0)" >> ${file}
        ;;
    2)
        echo ""
        echo "Custom shortcut option selected"
        echo " "
        create_shortcut
        ;;
    3)
        echo "Word option selected"
        echo " "
        read -p "Enter your word: " word
        echo -e "${custom_spaces}pyautogui.write('$word')" >> ${file}
        ;;
    *)
        echo "Wrong number"
        exit 1
esac
}

file="tmp_driver.py"

touch ${file}

for i in $(seq 0 99) ; do
    [ -e "/dev/ttyACM$i" ] && break
done

if [ ! -e "/dev/ttyACM$i" ]; then
    echo "No Arduino found"
    exit
fi


cat > ${file} << "EOF"
import pyautogui
import serial
import webbrowser

EOF

echo "arduino = serial.Serial(port='/dev/ttyACM${i}', baudrate=9600)" >> ${file}

cat >> ${file} << "EOF"
while True:
    action = arduino.read(1).decode('ascii')
    if action == 'u':
EOF


echo "Setting up button"
echo " "
echo "Enter number of option you would like:"
echo " "
echo "1. Open url"
echo "2. Custom shortcut"
echo "3. Custom word"

read -n 1 option
echo " "
select_opt $option




cat >> ${file} << "EOF"
    elif action == 'd':
EOF

echo "Setting down button"
echo " "
echo "Enter number of option you would like:"
echo " "
echo "1. Open url"
echo "2. Custom shortcut"
echo "3. Custom word"

read -n 1 option
echo " "
select_opt $option

mv driver.py old_driver.py

mv tmp_driver.py driver.py
